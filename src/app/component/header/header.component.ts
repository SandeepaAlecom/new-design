import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    $(document).ready(function(){
      console.log('here');
      $('.dropdown-submenu a.test').on("click", function(e){
      console.log('here 2');
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
      });
    });
  }

  onFocus() {
    document.getElementById('dropdown-content').style.display = 'block';
  }

  onBlur() {
    document.getElementById('dropdown-content').style.display = 'none';
  }
}

function setDisplay(num) {
  if(num == 1) {
      document.getElementById('online').style.display = 'block'
      document.getElementById('status').style.display = 'none'
  } else if (num == 2) {
      document.getElementById('online').style.display = 'none'
      document.getElementById('status').style.display = 'block'
  }
}

